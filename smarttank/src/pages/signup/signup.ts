import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

import {LoginPage} from '../login/login';
// import { TabsPage} from '../tabs/tabs';
import { DataProvider } from '../../providers/data';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  records : Data;
  
  vuser_id:Number;
  vfname:string;
  vlname:string;
  vemail:string;
  vpassword:string;
  vdevice_id:string;
  vComPassword:string;
  
  
  constructor(public navCtrl: NavController, public navParams: NavParams,private ds:DataProvider, public alertCtrl:AlertController) {
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
   
  }
  signup() {
    this.records = {
      fname:this.vfname,
      lname:this.vlname,
      email:this.vemail,
      password:this.vpassword,
      device_id:this.vdevice_id
    };
    this.ds.checkEmail(this.vemail).subscribe((res)=>{
      console.log(res);
      if(this.vpassword != this.vComPassword){
        const alert = this.alertCtrl.create({
          title: '',
          subTitle: 'Password Not Match',
          buttons: ['OK']
        });
        alert.present();
      }
      if(res[0].fld_email != undefined){
        const alert = this.alertCtrl.create({
          title: '',
          subTitle: 'Email Already Exist',
          buttons: ['OK']
        });
        alert.present();
      }else if (res[0].status=="404" && res[0].message=="No Data Found"){
        this.ds.sendData(this.records).subscribe((response)=>{
          const alert = this.alertCtrl.create({
            title: 'Success!',
            subTitle: 'Sign up Sucessfully!!',
            buttons: ['OK']
          });
          alert.present();
        });
        this.navCtrl.push(LoginPage);

      }
    });
    
  }
  
}
interface Data{
  fname:string,
  lname:string,
  email:string,
  password:string,
  device_id:string
}
