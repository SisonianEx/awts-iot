import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams,AlertController, Alert,App } from 'ionic-angular';
import { NavController, NavParams,AlertController, App } from 'ionic-angular';
import {DataProvider} from '../../providers/data';

/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  fname:string;
  lname:string;
  email:string;
  deviceId:string;
  vCurrentPass:string;
  vNewPass:string;
  vConfirmNewPass:string;
id:Number;
pass:string;

info :UpdateInfo;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl:AlertController, public app:App, private ds:DataProvider ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');
    this.id = parseInt(localStorage.getItem("userid"));
    this.fetch();
  }

  fetch(){
    this.ds.getWithId(this.id).subscribe((res)=>{
      console.log(res);
      this.fname = res[0].fld_fname;
      this.lname = res[0].fld_lname;
      this.email = res[0].fld_email;
      this.deviceId = res[0].fld_device_id;
    });
  }

  updatePass(){
    this.id = parseInt(localStorage.getItem("userid"));
    this.pass = localStorage.getItem("password");
    if(this.vCurrentPass != this.pass){
      const alert = this.alertCtrl.create({
        title: '',
        subTitle: ' Current Password Did Not Match!',
        buttons: ['OK']
      });
      alert.present();
    }else if(this.vConfirmNewPass != this.vNewPass ){
      const alert = this.alertCtrl.create({
        title: '',
        subTitle: 'New Password Dont Match!',
        buttons: ['OK']
      });
      alert.present();
    }else {
   this.info = {
     user_id: this.id,
     password: this.vConfirmNewPass
   };
   this.ds.UpdateUser(this.info).subscribe((res)=>{
    const alert = this.alertCtrl.create({
      title: '',
      subTitle: 'Updated Successfully!',
      buttons: ['OK']
    });
    alert.present();
   });
  }

  }

  logoutme() {
    //this.navCtrl.push(WelcomePage);
    const root = this.app.getRootNav();
    
    const confirm = this.alertCtrl.create({
      title: 'Exit',
      message: 'Are you sure you want to log out?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            // console.log('Agree clicked');
            root.popToRoot();
          }
        }
      ]
    });
    confirm.present();
  }
 

}
interface UpdateInfo{
  user_id:Number,
  password:string
}
