import { Component } from '@angular/core';
import { NavController,App, AlertController } from 'ionic-angular';
import { DataProvider } from '../../providers/data';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  dynamicHeight: string = "250px";
  number: any;
  percentage: any;
  device_id: any;

  constructor(public navCtrl: NavController, public app:App,public alertCtrl:AlertController, public dataProvider: DataProvider) {
    
  }

  ionViewDidLoad() {
    this.viewData();
   
  } 
  
  randomInt(min, max){
    this.number = Math.floor(Math.random() * (max - min + 1)) + min;
    this.dynamicHeight = this.number+"px";
    this.percentage = ((this.number/420)*100).toFixed(0);
 }
 random(){
   this.randomInt(0,420);
 }
  logoutme() {
    //this.navCtrl.push(WelcomePage);
    const root = this.app.getRootNav();
    
    const confirm = this.alertCtrl.create({
      title: 'Exit',
      message: 'Are you sure you want to log out?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            // console.log('Agree clicked');
            root.popToRoot();
          }
        }
      ]
    });
    confirm.present();
  }
 
  viewData() {
    this.device_id = localStorage.getItem('device_id');
    this.dataProvider.getData(this.device_id).subscribe((response)=>{
      // this.records = response;
      // console.log(response);
        console.log(response[0].fld_water_level);
        this.dynamicHeight = (response[0].fld_water_level*10)+"px";
        this.percentage = (((response[0].fld_water_level*10)/440)*100).toFixed(0);
        if(response[0].fld_water_level*10 > 440){
          this.percentage = 100;
        } else if(response[0].fld_water_level*10 < 0) {
          this.percentage = 0;
        } else if(response[0].status=='404'){
          this.percentage = 0;
          this.dynamicHeight = "0px";
        }
      setTimeout( () => {
        this.viewData();
      }, 1000);
    });
  }
}
