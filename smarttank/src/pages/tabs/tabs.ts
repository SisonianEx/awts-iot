import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';  

import { AboutPage } from '../about/about';
import { SettingPage } from '../setting/setting';
import { HomePage } from '../home/home';  
import { HelpPage } from '../help/help';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = SettingPage;
  tab4Root = HelpPage;

  

  constructor( public alertCtrl: AlertController) {

  }

}
