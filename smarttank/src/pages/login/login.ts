import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';

import {TabsPage} from '../tabs/tabs';
import { SignupPage } from '../signup/signup';
import {DataProvider} from '../../providers/data';
// import { HomePage } from '../home/home';
// import { SettingPage } from '../setting/setting';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
vusername:string;
vpassword:string;
login :Login;
public type = 'password';
public showPass = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private ds:DataProvider,public alertCtrl:AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  showPassword() {
    this.showPass = !this.showPass;
 
    if(this.showPass){
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }
  signin(){
    this.ds.verifylogin(this.vusername,this.vpassword).subscribe((res)=>{
      // console.log(res);
      if(this.vusername == res[0].fld_email && this.vpassword == res[0].fld_password){
        const alert = this.alertCtrl.create({
          title: 'Login Successfully!',
          subTitle: 'Welcome to AWTS '+this.vusername,
          buttons: ['OK']
        });
        this.navCtrl.push(TabsPage);
        alert.present();
        localStorage.setItem('userid',res[0].fld_user_id);
        localStorage.setItem('password',res[0].fld_password);
        localStorage.setItem('device_id',res[0].fld_device_id);
      }
      else if (res[0].status=="404" && res[0].message=="No Data Found"){
        const alert = this.alertCtrl.create({
          title: 'Invalid!',
          subTitle: 'Please check your Username/Email and Password!',
          buttons: ['OK']
        });
        alert.present();
      }
    });
   
  }
  signup(){
    
    this.navCtrl.push(SignupPage);
  }
  

}
interface Login{
  user_id:Number,
  email:string,
  password:string
}
