import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// import { SettingPage } from '../pages/setting/setting';
// import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
// import { HelpPage } from '../pages/help/help';
import { OneSignal } from '@ionic-native/onesignal';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public onesignal:OneSignal) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

  }

  processNotification(){
    this.onesignal.startInit('AIzaSyAZ2TW4uPRUO50Ha4gBhLC8iwydt651VIs','765680818208');
    this.onesignal.inFocusDisplaying(this.onesignal.OSInFocusDisplayOption.InAppAlert);

    this.onesignal.handleNotificationReceived().subscribe((res)=>{
      console.log(res);
    });
    this.onesignal.endInit;
  }
  saveData(res){
    var title = res.payload;
    var payload = res.payload.body;
    localStorage.setItem('payload',payload);
    localStorage.setItem('title',title);
  }
}


