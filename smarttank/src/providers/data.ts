import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataProvider {
  post:String;
  constructor(public http: Http) {
    console.log('Hello DataProvider Provider');
  }

  id:string;

  getData(id){
    // return this.http.get('https://api.thingspeak.com/channels/539316/feeds.json?api_key=UWZ9BXF83EY7VZ7S&results=1').map((res) => res.json());
    // return this.http.get('http://192.168.1.11/arduinoSample/sampledata.php').map((res) => res.json());
    return this.http.get('http://gordoncollegeccs-ssite.net/singhjagjit/sisonian/awts_api/latest_data/tbl_sensor_data/fld_device_id/'+id+'/fld_id/DESC/1').map((res) => res.json());
  }
  getWithId(id){
    return this.http.get('http://gordoncollegeccs-ssite.net/singhjagjit/sisonian/awts_api/read/tbl_users/fld_user_id/'+id).map((res) => res.json());
  }
  sendData(data){
    this.post = JSON.stringify(data);
    // console.log(this.post);
    return this.http.post('http://gordoncollegeccs-ssite.net/singhjagjit/sisonian/awts_api/create/tbl_users',this.post).map((res) => res.json());
  }
  verifylogin(username,password){
    // this.post = JSON.stringify(data);
    return this.http.get('http://gordoncollegeccs-ssite.net/singhjagjit/sisonian/awts_api/login/'+username+'/'+password).map((res) => res.json());
    
  }
  UpdateUser(data){
    this.post = JSON.stringify(data);
    console.log(this.post);
    return this.http.post('http://gordoncollegeccs-ssite.net/singhjagjit/sisonian/awts_api/update/tbl_users',this.post).map((res) => res.json());
  }
  checkEmail(inputEmail){
    return this.http.get('http://gordoncollegeccs-ssite.net/singhjagjit/sisonian/awts_api/checkEmail/'+inputEmail).map((res) => res.json());
  }

}
